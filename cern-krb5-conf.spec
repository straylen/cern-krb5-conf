Name:		cern-krb5-conf
Version:	1.2
Release:	1%{?dist}
Summary:	A kerberos configuration file to be used at CERN
Group:		CERN/Utilities
License:	BSD
URL:		http://linux.cern.ch
Source0:        %{name}-%{version}.tgz
BuildArch:	noarch
Requires:	krb5-libs

%description
This rpm provides a basic kerberos configuration file to be used at CERN

%package -n cern-krb5-conf-atlas
Summary: A kerberos configuration file to be used at CERN (ATLAS)
Conflicts: cern-krb5-conf, cern-krb5-conf-cms, cern-krb5-conf-tn, cern-krb5-conf-ipadev
%description -n cern-krb5-conf-atlas
This rpm provides a basic kerberos configuration file to be used at CERN (ATLAS)

%package -n cern-krb5-conf-cms
Summary: A kerberos configuration file to be used at CERN (CMS)
Conflicts: cern-krb5-conf, cern-krb5-conf-atlas, cern-krb5-conf-tn, cern-krb5-conf-ipadev
%description -n cern-krb5-conf-cms
This rpm provides a basic kerberos configuration file to be used at CERN (CMS)

%package -n cern-krb5-conf-tn
Summary: A kerberos configuration file to be used at CERN (TN)
Conflicts: cern-krb5-conf, cern-krb5-conf-atlas, cern-krb5-conf-cms, cern-krb5-conf-ipadev
%description -n cern-krb5-conf-tn
This rpm provides a basic kerberos configuration file to be used at CERN (TN)

%package -n cern-krb5-conf-ipadev
Summary: A Kerberos configuration file to be used for CERN's FreeIPA development realm
Conflicts: cern-krb5-conf, cern-krb5-conf-cms, cern-krb5-conf-atlas, cern-krb5-conf-tn
%description -n cern-krb5-conf-ipadev
This RPM provides a basic Kerberos configuration file to be used for CERN's 
FreeIPA development realm

%prep
%setup -q

%build
%install

install -d %{buildroot}/%{_sysconfdir}/krb5.conf.d
install -p -m 0644 cern.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern.conf
install -p -m 0644 cern-atlas.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-atlas.conf
install -p -m 0644 cern-cms.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-cms.conf
install -p -m 0644 cern-tn.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-tn.conf
install -p -m 0644 cern-ipadev.conf %{buildroot}/%{_sysconfdir}/krb5.conf.d/cern-ipadev.conf

%files
%{_sysconfdir}/krb5.conf.d/cern.conf

%files -n cern-krb5-conf-atlas
%{_sysconfdir}/krb5.conf.d/cern-atlas.conf

%files -n cern-krb5-conf-cms
%{_sysconfdir}/krb5.conf.d/cern-cms.conf

%files -n cern-krb5-conf-tn
%{_sysconfdir}/krb5.conf.d/cern-tn.conf

%files -n cern-krb5-conf-ipadev
%{_sysconfdir}/krb5.conf.d/cern-ipadev.conf

%changelog

* Mon May 11 2020 Julien Rische <julien.rische@cern.ch> 1.2-1
- Add subpackage for FreeIPA development realm

* Tue Jan 28 2020 Ben Morrice <ben.morrice@cern.ch> 1.1-1
- add subpackages to provide krb5.conf for atlas,cms,tn

* Tue Jan 14 2020 Ben Morrice <ben.morrice@cern.ch> 1.0-1
- Initial release
